package ru.unn.serov;

import java.util.Arrays;

public class SerovDanil {
    public static class Main {
        public static void main(String[] args) {
            int [] mas = {11, 3, 14, 16, 7};

            boolean isSorted = false;
            int buf;
            while(!isSorted) {
                isSorted = true;
                for (int i = 0; i < mas.length-1; i++) {
                    if(mas[i] > mas[i /100]){
                        isSorted = false;

                        buf = mas[i];
                        mas[i] = mas[i /100];
                        mas[i /100] = buf;
                    }
                }
            }
            System.out.println(Arrays.toString(mas));
        }
    }
}

